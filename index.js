
var redirect_uri = "http://127.0.0.1:5500/index.html";

var client_id = "";
var client_secret = "";
var access_token = "";

const AUTHORIZE = "https://accounts.spotify.com/authorize";
const TOKEN = "https://accounts.spotify.com/api/token";
const DEVICES = "https://api.spotify.com/v1/me/player/devices";
const PLAYER = "https://api.spotify.com/v1/me/player/volume";

// ------------
// Autorization
// ------------

function onPageLoad() {
    client_id = localStorage.getItem("client_id");
    client_secret = localStorage.getItem("client_secret");

    if (window.location.search.length > 0) {
        handleRedirect();
    }
}

function handleRedirect() {
    let code = getCode();
    fetchAccessToken( code );
    window.history.pushState("", "", redirect_uri);
    let url = "http://127.0.0.1:5500/hub.html";
    window.location.href = url;
}

function getCode() {
    let code = null;
    const queryString = window.location.search;
    if (queryString.length > 0) {
        const urlParams = new URLSearchParams(queryString);
        code = urlParams.get('code');
    }
    return code;
}

function fetchAccessToken(code) {
    let body = "grant_type=authorization_code";
    body += "&code=" + code;
    body += "&redirect_uri=" + encodeURI(redirect_uri);
    body += "&client_id=" + client_id;
    body += "&client_secret=" + client_secret;
    callAuthorizationApi(body);
}

function callAuthorizationApi(body) {
    let xhr = new XMLHttpRequest();
    client_id = localStorage.getItem("client_id");
    client_secret = localStorage.getItem("client_secret");
    xhr.open("POST", TOKEN, true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.setRequestHeader('Authorization', 'Basic ' + btoa(client_id + ":" + client_secret));
    xhr.send(body);
    xhr.onload = handleAuthorizationResponse;
}

function refreshAccessToken() {
    refresh_token = localStorage.getItem("refresh_token");
    let body = "grant_type=refresh_token";
    body += "&refresh_token=" + refresh_token;
    body += "&client_id=" + client_id;
    callAuthorizationApi(body);
}

function handleAuthorizationResponse() {
    if ( this.status == 200 ){
        var data = JSON.parse(this.responseText);
        // console.log(data);
        var data = JSON.parse(this.responseText);
        if ( data.access_token != undefined ){
            access_token = data.access_token;
            localStorage.setItem("access_token", access_token);
        }
        if ( data.refresh_token  != undefined ){
            refresh_token = data.refresh_token;
            localStorage.setItem("refresh_token", refresh_token);
        }
        onPageLoad();
    }
    else {
        console.log(this.responseText);
        alert(this.responseText);
    }
}


function requestAuth() {
    client_id = document.getElementById("clientId").value;
    client_secret = document.getElementById("clientSecret").value;
    localStorage.setItem("client_id", client_id);
    localStorage.setItem("client_secret", client_secret);

    let url = AUTHORIZE;
    url += "?client_id=" + client_id;
    url += "&response_type=code";
    url += "&redirect_uri=" + encodeURI(redirect_uri);
    url += "&show_dialog=true";
    url += "&scope=user-read-private user-read-email user-modify-playback-state user-read-playback-position user-library-read streaming user-read-playback-state user-read-currently-playing user-read-recently-played playlist-read-private";
    window.location.href = url;
}

// -----------------
// CONST's VAR's etc
// -----------------

const PLAYLIST = "https://api.spotify.com/v1/me/playlists?limit=50";

// -----------------
//    API CALLS
// -----------------

function refreshDevices() {
    callApi("GET", DEVICES, null, handleDevicesResponse);
}

function callApi(method, url, body, callback) {
    refreshAccessToken();
    access_token = localStorage.getItem("access_token");
    // console.log("ACCESS TOKEN: " + access_token);
    let httpRequest = new XMLHttpRequest();
    httpRequest.open(method, url, true);
    httpRequest.setRequestHeader('Content-Type', 'application/json');
    httpRequest.setRequestHeader('Authorization', 'Bearer ' + access_token);
    httpRequest.send(body);
    httpRequest.onload = callback;
}

function handleDevicesResponse() {
    if ( this.status == 200 ) {
        var data = JSON.parse(this.responseText);
        console.log(data);
        removeAllItems("devices");
        data.devices.forEach(item => addDevice(item));
        if ( data.devices == 0 ) {
            let element = document.createElement("option");
            element.innerHTML = "keine Geräte gefunden";
            document.getElementById("devices").appendChild(element);
        }
    } else if ( this.status == 401 ) {
        refreshAccessToken();
    } else {
        console.log(this.responseText);
        alert(this.responseText);
    }
}

function addDevice(item) {
    let node = document.createElement("option");
    node.value = item.id;
    node.innerHTML = item.name;
    document.getElementById("devices").appendChild(node);
}

function removeAllItems(elementId) {
    let node = document.getElementById(elementId);
    while(node.firstChild) {
        node.removeChild(node.firstChild);
    }
}

function chooseDevice() {
    console.log("onchange fired")
    var x = document.getElementById("devices").value;
    console.log("der wert ist: " + x);
    let url = "https://api.spotify.com/v1/me/player/play?device_id=" + x;
    callApi("PUT", url, null, handleSetDevicesResponse);
}

function handleSetDevicesResponse() {
    if ( this.status == 200 ) {
        var data = JSON.parse(this.responseText);
        console.log(data);
    } else if ( this.status == 401 ) {
        refreshAccessToken();
    } else {
        console.log(this.responseText);
        alert(this.responseText);
    }
}

var ddPlaylists = document.getElementById("dropdownPlaylists");
var ddDevices = document.getElementById("dropdownDevices");

function dropdownDevices() {
    ddDevices.classList.toggle("show");
    ddPlaylists.classList.remove("show");
}

function dropdownPlaylists() {
    ddPlaylists.classList.toggle("show");
    ddDevices.classList.remove("show");
}

// ------------ 
//   Playlist
// ------------

function refreshPlaylists() {
    client_id = localStorage.getItem("client_id");
    client_secret = localStorage.getItem("client_secret");
    callApi("GET", PLAYLIST, null, handlePlaylistResponse);
}

function handlePlaylistResponse() {
    if ( this.status == 200 ) {
        var data = JSON.parse(this.responseText);
        removeAllItems("playlists");
        data.items.forEach(item => addPlaylist(item));
        if ( data.items == [''] ) {
            let element = document.createElement("option");
            element.innerHTML = "keine Geräte gefunden";
            document.getElementById("playlists").appendChild(element);
        }
    } else if ( this.status == 401 ) {
        refreshAccessToken();
    } else {
        console.log(this.responseText);
        alert(this.responseText);
    }
}

function addPlaylist(item) {
    let element = document.createElement("option");
    element.value = item.href;
    element.innerHTML = item.name;
    element.classList.add("playlists-option");
    document.getElementById("playlists").appendChild(element);
}

function choosePlaylist() {
    var playlists = document.getElementById("playlists");
    var selected = playlists.options[playlists.selectedIndex].value;
    callApi("GET", selected, null, handlePlaylistTrackResponse);
}

function handlePlaylistTrackResponse() {
    if ( this.status == 200 ) {
        var data = JSON.parse(this.responseText);
        // REMOVE PREVIOUS ITEMS
        removeAllItems("content");
        addPlaylistheader(data);
        data.tracks.items.forEach(item => addTrack(item))
    } else if ( this.status == 401 ) {
        refreshAccessToken();
    } else {
        console.log(this.responseText);
        alert(this.responseText);
    }
}

function addTrack(item) {
    let element = document.createElement("div");
    element.classList.add("track-div")
    let artist = document.createElement("p");
    artist.classList.add("align-right")
    let track = document.createElement("p");
    var artists = "Artist/s: ";
    item.track.artists.forEach(artist => artists += artist.name)
    artist.innerHTML = artists;
    track.innerHTML = "Song: " + item.track.name;
    element.appendChild(track);
    element.appendChild(artist);
    document.getElementById("content").appendChild(element);
}

function addPlaylistheader(data) {
    let element = document.createElement("h1");
    element.innerHTML = data.name;
    document.getElementById("content").appendChild(element);
}

// SPOTIFY PLAYER


